const flatPrint = arr => console.log(arr.join(''));

const trace = arr => {
    flatPrint(arr);
    return arr;
};

const noDups = arr => {
    const noZero = arr.filter(x => x != 0);
    return new Set(noZero).size === noZero.length;
};

const getRow = n => arr => arr.slice(n * 9, n * 9 + 9);

const getRow0 = getRow(0);
const getRow1 = getRow(1);
const getRow2 = getRow(2);
const getRow3 = getRow(3);
const getRow4 = getRow(4);
const getRow5 = getRow(5);
const getRow6 = getRow(6);
const getRow7 = getRow(7);
const getRow8 = getRow(8);

const getCol = n => arr => [
    arr[n + 0],
    arr[n + 9],
    arr[n + 18],
    arr[n + 27],
    arr[n + 36],
    arr[n + 45],
    arr[n + 54],
    arr[n + 63],
    arr[n + 72]
];

const getCol0 = getCol(0);
const getCol1 = getCol(1);
const getCol2 = getCol(2);
const getCol3 = getCol(3);
const getCol4 = getCol(4);
const getCol5 = getCol(5);
const getCol6 = getCol(6);
const getCol7 = getCol(7);
const getCol8 = getCol(8);

const blockStart = [0, 3, 6, 27, 30, 33, 54, 57, 60];

const getBlock = n => arr => {
    const b = blockStart[n];

    return arr.slice(b, b + 3)
	.concat(arr.slice(b + 9, b + 12))
	.concat(arr.slice(b + 18, b + 21));
}

const getBlock0 = getBlock(0);
const getBlock1 = getBlock(1);
const getBlock2 = getBlock(2);
const getBlock3 = getBlock(3);
const getBlock4 = getBlock(4);
const getBlock5 = getBlock(5);
const getBlock6 = getBlock(6);
const getBlock7 = getBlock(7);
const getBlock8 = getBlock(8);

const isValidGrid = arr =>
      noDups(getRow0(arr)) &&
      noDups(getRow1(arr)) &&
      noDups(getRow2(arr)) &&
      noDups(getRow3(arr)) &&
      noDups(getRow4(arr)) &&
      noDups(getRow5(arr)) &&
      noDups(getRow6(arr)) &&
      noDups(getRow7(arr)) &&
      noDups(getRow8(arr)) &&
      noDups(getCol0(arr)) &&
      noDups(getCol1(arr)) &&
      noDups(getCol2(arr)) &&
      noDups(getCol3(arr)) &&
      noDups(getCol4(arr)) &&
      noDups(getCol5(arr)) &&
      noDups(getCol6(arr)) &&
      noDups(getCol7(arr)) &&
      noDups(getCol8(arr)) &&
      noDups(getBlock0(arr)) &&
      noDups(getBlock1(arr)) &&
      noDups(getBlock2(arr)) &&
      noDups(getBlock3(arr)) &&
      noDups(getBlock4(arr)) &&
      noDups(getBlock5(arr)) &&
      noDups(getBlock6(arr)) &&
      noDups(getBlock7(arr)) &&
      noDups(getBlock8(arr));

const incIndex = (i, arr) =>
      arr.slice(0, i).concat(arr[i] + 1).concat(arr.slice(i + 1));

const firstEmpty = arr => arr.indexOf(0);

export const findSolution = arr => {
    const i = firstEmpty(arr);

    if(i === -1) {
	if(isValidGrid(arr))
	    return arr;
	else
	    return null;
    }

    let result = null;
    let incArr = [...arr];

    while (result === null && incArr[i] < 9) {
	incArr = incIndex(i, incArr);

	//if(isValidGrid(trace(incArr)))
	if(isValidGrid(incArr))
	    result = findSolution(incArr);
    }

    return result;
}

export const prettyPrint = arr => {
    console.log(arr.slice(0, 9));
    console.log(arr.slice(9, 18));
    console.log(arr.slice(18, 27));
    console.log(arr.slice(27, 36));
    console.log(arr.slice(36, 45));
    console.log(arr.slice(45, 54));
    console.log(arr.slice(54, 63));
    console.log(arr.slice(63, 72));
    console.log(arr.slice(72, 81));
    console.log();
}
